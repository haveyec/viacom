//
//  HtmlClean.swift
//  ViacomProject
//
//  Created by Henry, Marlon on 7/29/15.
//  Copyright (c) 2015 Nerd Gang LLC. All rights reserved.
//

import UIKit
extension String{
   mutating func removeHtmlCharFromString() -> String{
var tempString = self
        tempString = tempString.stringByReplacingOccurrencesOfString("&#038;", withString: "&")
    tempString = tempString .stringByReplacingOccurrencesOfString("&#8217;", withString: "'", options: nil, range: nil)
    tempString = tempString .stringByReplacingOccurrencesOfString("&#8220;", withString: "\" ", options: nil, range: nil)
    tempString = tempString .stringByReplacingOccurrencesOfString("&#8221;", withString: "\" ", options: nil, range: nil)
    tempString = tempString .stringByReplacingOccurrencesOfString("&#8230;", withString: "..." , options: nil, range: nil)
    tempString = tempString.stringByReplacingOccurrencesOfString("&#8211;", withString: "-", options: nil, range: nil)
    
        return tempString
    }
    
}

class HtmlClean: NSObject {
    
   
}
