//
//  WebServices.swift
//  ViacomProject
//
//  Created by Havic on 5/29/15.
//  Copyright (c) 2015 Nerd Gang LLC. All rights reserved.
//

import UIKit
protocol jsonDelegate{
    func reloadTable()
}

class WebServices: NSObject {
    var jsonUrl = NSURL(string: "http://grownandtechie.com/api/get_posts/?count=20")
    var session = NSURLSession.sharedSession()
    var blogArray = NSMutableArray()
    var blogImageUrl = NSURL()
    var reloadJsonDelegate = jsonDelegate?()
    
    /**
    
    Json Loader Method the downloading of json and the values being passed in from the json Object
    
    
    :param: dataTask -> shared session object dataTaskWithUrl(NSURL!,{ (NSData, NSResponse, NSError)} )
    
    
    :returns: The blogArray which is created from when we temporaily stored it in tempArr
    
    */
    func loadThatJson(){
        
        
        var dataTask = session.dataTaskWithURL(jsonUrl!, completionHandler: { (data, response, error) -> Void in
            var jsonDic = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.AllowFragments, error: nil) as! NSDictionary
            var tempArr = jsonDic["posts"] as! NSArray
            for posts in tempArr as! [NSDictionary]{
                var blogData = JsonObject()
                blogData.blogTitle = posts["title"] as? String
                blogData.blogTitle = blogData.blogTitle?.removeHtmlCharFromString()
                blogData.date = posts["date"] as?  NSMutableString
                var authors = posts["author"] as! NSDictionary
                blogData.author = authors["name"] as? String
                blogData.thumbnail = posts["thumbnail"] as? String
                blogData.articleUrl = posts["url"] as? String
                
                self.blogArray.addObject(blogData)
            }
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                self.reloadJsonDelegate?.reloadTable()
            })
        })
        dataTask.resume()
    }
   
}
