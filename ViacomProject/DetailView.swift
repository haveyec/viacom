//
//  DetailView.swift
//  ViacomProject
//
//  Created by Havic on 6/1/15.
//  Copyright (c) 2015 Nerd Gang LLC. All rights reserved.
//

import UIKit

class DetailView: UIViewController {
    var strArticleUrl: NSString = NSString()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        var articleUrl = NSURL(string: strArticleUrl as String)
        var request = NSURLRequest(URL: articleUrl!)
        myWebView.loadRequest(request)
    }
@IBOutlet weak var myWebView: UIWebView!
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
