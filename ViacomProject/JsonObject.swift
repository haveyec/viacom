//
//  JsonObject.swift
//  ViacomProject
//
//  Created by Havic on 5/29/15.
//  Copyright (c) 2015 Nerd Gang LLC. All rights reserved.
//

import UIKit

class JsonObject: NSObject {
    var blogTitle: String?
    var author: String?
    var thumbnail: String?
    var date = NSMutableString?()
    var articleUrl = String?()
    
}
