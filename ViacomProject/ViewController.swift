//
//  ViewController.swift
//  ViacomProject
//
//  Created by Havic on 5/29/15.
//  Copyright (c) 2015 Nerd Gang LLC. All rights reserved.
//

import UIKit

class ViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,jsonDelegate {
    @IBOutlet weak var myTable: UITableView!
    var blogPosts = WebServices()
    var imageData = NSData()
    var date = NSMutableString()
    var fullStory = DetailView()
    

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.blogPosts.reloadJsonDelegate = self
        self.blogPosts.loadThatJson()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func reloadTable() {
        myTable.reloadData()
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // Return the number of rows in the section.
        return blogPosts.blogArray.count
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as! UITableViewCell
        var blogContent = self.blogPosts.blogArray.objectAtIndex(indexPath.row) as! JsonObject
        var defaultImage = UIImage(named: "logoGNT")
        date = blogContent.date!
        
        
        
        // Multithreaded Method for downloading of the images using GCD(Grand Central Dispatch)
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), { () -> Void in
            let blogImage = blogContent.thumbnail
            
            if (blogImage != nil){
                self.blogPosts.blogImageUrl = NSURL(string: blogImage! as NSString as String)!
                self.imageData = NSData(contentsOfURL: self.blogPosts.blogImageUrl)!
                var currentImage: UIImage = UIImage(data: self.imageData)!
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    cell.imageView!.image = currentImage
                })
            }
        })
        
        
        
        // Configure the cell...
        cell.textLabel?.text = blogContent.blogTitle
        cell.detailTextLabel?.text = "\(blogContent.author!) on \(formattedDate())"
        cell.imageView?.image = UIImage(named: "logoGNT")
        
        return cell
    }
    
    // MARK: - Segue Method
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        var storyCell = sender as! UITableViewCell
        fullStory = segue.destinationViewController as! DetailView
        var indexPathVar: NSIndexPath = myTable.indexPathForCell(storyCell)!
        fullStory.strArticleUrl = (blogPosts.blogArray.objectAtIndex(indexPathVar.row) as! JsonObject).articleUrl!
    }
    
    
    /**
    
    Method to format the way the date looks
    
    :param:
    
    :returns: Date formated as Day, Month, Date and full year
    */
    
    
    func formattedDate() -> String {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let tempDate = dateFormatter.dateFromString(date as String)
        dateFormatter.dateFormat = "EE MMM dd, yyyy"
        return dateFormatter.stringFromDate(tempDate!)
    }
    


}

